package com.amazon.arsv.cdmailer;

import static com.amazon.arsv.mailutils.Constants.CD_HACKATHON_MEETING;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;

import com.amazon.arsv.mailutils.MailUtils;
import com.amazon.arsv.mailutils.ServiceUtils;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.util.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListThreadsResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;
import com.google.api.services.gmail.model.ModifyMessageRequest;
import com.google.api.services.gmail.model.Thread;
import com.google.common.collect.Lists;

public class GmailCDHackathonMeetingApp {

	public static void main(String[] args) throws Exception {

		while(true) {
			try {
			runApp();
			java.lang.Thread.sleep(2000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	public static void runApp() throws Exception {
		
		Credential credential = MailUtils.getToken(CD_HACKATHON_MEETING);

		Gmail service = MailUtils.createEmailClient(credential);

		// Retrieve a page of Threads; max of 100 by default.
		List<String> labelsList = Lists.newArrayList("UNREAD", "INBOX");
		com.google.api.services.gmail.Gmail.Users.Threads.List tList = service
				.users().threads().list(CD_HACKATHON_MEETING);
		tList.setLabelIds(labelsList);

		ListThreadsResponse threadsResponse = tList.execute();
		List<Thread> threads = threadsResponse.getThreads();
		if (threads == null) {
			threads = Lists.newArrayList();
		}

		for (Thread t : threads) {
			System.out.println(t.toPrettyString());
		}

		for (Thread thread : threads) {
			System.out.println("Thread ID: " + thread.getId());
			Thread t = service.users().threads()
					.get(CD_HACKATHON_MEETING, thread.getId()).execute();
			String subject = "";
			String date = "";
			String from = "";
			String to = "";

			for (Message m : t.getMessages()) {

				System.out.println(m.toPrettyString());

				for (MessagePartHeader h : m.getPayload().getHeaders()) {
					if (h.getName().equals("Subject")) {
						System.out.println("Subject: " + h.getValue());
						subject = h.getValue();
						subject = StringUtils.replace(subject, " ", "_");
					}
					if (h.getName().equals("From")) {
						System.out.println("From: " + h.getValue());
						from = h.getValue();
					}
					if (h.getName().equals("To")) {
						System.out.println("To: " + h.getValue());
						to = h.getValue();
					}
					if (h.getName().equals("Date")) {
						System.out.println("Date: " + h.getValue());
						date = h.getValue();
					}
				}

				// get title and userId to send to service and get token
				String fromUserId = StringUtils
						.substringBetween(from, "<", ">");
				fromUserId = (fromUserId == null) ? from : fromUserId;
				String snippet = (StringUtils.isBlank(subject)) ? date
						: subject;
				String token = ServiceUtils.getToken(fromUserId, snippet);

				// Construct mailto:link and send reply
				String replyTemplate = "Thank you for using CloudDrive-Notes. Please use this id <%s> to email notes and other attachments.";
				String updatedEmail = "cdhackathon+" + token + "@gmail.com";
				String replyString = String.format(replyTemplate, updatedEmail);
				Properties props = new Properties();
				Session session = Session.getDefaultInstance(props, null);
				MimeMessage email = new MimeMessage(session);
				InternetAddress tAddress = new InternetAddress(from);
				InternetAddress fAddress = new InternetAddress(
						CD_HACKATHON_MEETING);
				email.setFrom(fAddress);
				email.addRecipient(javax.mail.Message.RecipientType.TO,
						tAddress);
				email.setSubject("Re:" + subject);
				email.setText(replyString);

				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				email.writeTo(bytes);
				String encodedEmail = Base64.encodeBase64URLSafeString(bytes
						.toByteArray());
				Message replyMessage = new Message();
				replyMessage.setRaw(encodedEmail);

				Message sentMsg = service.users().messages()
						.send(CD_HACKATHON_MEETING, replyMessage).execute();
				System.out
						.println("Reply Message: " + sentMsg.toPrettyString());

				// mark message as read
				List<String> removeLabels = Lists.newArrayList("UNREAD",
						"INBOX", "TRASH");
				ModifyMessageRequest modmsgreq = new ModifyMessageRequest()
						.setRemoveLabelIds(removeLabels);
				Message message = service.users().messages()
						.modify(CD_HACKATHON_MEETING, m.getId(), modmsgreq)
						.execute();
				System.out.println("Marked as read : " + message.getId());

			}

		}
		
	}
}
