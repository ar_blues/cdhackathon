package com.amazon.arsv.cdmailer;

import static com.amazon.arsv.mailutils.Constants.CD_HACKATHON;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.amazon.arsv.mailutils.MailUtils;
import com.amazon.arsv.mailutils.ServiceUtils;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.util.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListThreadsResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.google.api.services.gmail.model.MessagePartHeader;
import com.google.api.services.gmail.model.ModifyMessageRequest;
import com.google.api.services.gmail.model.Thread;
import com.google.common.collect.Lists;

public class GmailCDHackathonApp {

	public static void main(String[] args) throws Exception {

		while(true) {
			try {
				runApp();
				java.lang.Thread.sleep(2000);
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
		

	}
	
	public static void runApp() throws Exception {
		Credential credential = MailUtils.getToken(CD_HACKATHON);

		Gmail service = MailUtils.createEmailClient(credential);

		// Retrieve a page of Threads; max of 100 by default.
		List<String> labelsList = Lists.newArrayList("UNREAD", "INBOX");
		com.google.api.services.gmail.Gmail.Users.Threads.List tList = service
				.users().threads().list(CD_HACKATHON);
		tList.setLabelIds(labelsList);

		ListThreadsResponse threadsResponse = tList.execute();
		List<Thread> threads = threadsResponse.getThreads();
		if (threads == null) {
			threads = Lists.newArrayList();
		}

		// Print ID of each Thread.
		for (Thread thread : threads) {
			System.out.println("Thread ID: " + thread.getId());
			Thread t = service.users().threads()
					.get(CD_HACKATHON, thread.getId()).execute();
			String subject = "";
			String date = "";
			String from = "";
			String from_prefix = "";
			String from_prefix_trim= "";
			String to = "";
			List<File> files = Lists.newArrayList();

			for (Message m : t.getMessages()) {

				for (MessagePartHeader h : m.getPayload().getHeaders()) {
					if (h.getName().equals("Subject")) {
						System.out.println("Subject: " + h.getValue());
						subject = h.getValue();
						subject = StringUtils.replace(subject, " ", "_");
					}
					if (h.getName().equals("From")) {
						System.out.println("From: " + h.getValue());
						from = h.getValue();
					}
					if (h.getName().equals("To")) {
						System.out.println("To: " + h.getValue());
						to = h.getValue();
					}
					if (h.getName().equals("Date")) {
						System.out.println("Date: " + h.getValue());
						date = h.getValue();
					}
				}
				from_prefix_trim = StringUtils.substringBetween(from, "<", ">");
				from_prefix = (from_prefix_trim==null) ? from : StringUtils.substringBefore(from_prefix_trim, "@");
				from_prefix = from_prefix+"_" + subject;
				System.out.println("from_prefixL " + from_prefix);

				for (MessagePart mp : m.getPayload().getParts()) {

					System.out.println(mp.toPrettyString());

					if (mp.getMimeType().endsWith("plain")) {
						File tempDir = new File("/tmp/" + subject + "/");
						if (!tempDir.exists()) {
							// TODO: add date and from in this field
							FileUtils.forceMkdir(tempDir);
						}
						File bodyFile = new File(tempDir, from_prefix+"_body.txt");
						FileWriter fw = new FileWriter(bodyFile);
						fw.write(new String(Base64.decodeBase64(mp.getBody()
								.getData()), "UTF-8"));
						fw.close();
						files.add(bodyFile);
						System.out.println("Created File : "
								+ bodyFile.getPath());
					} else if (mp.getMimeType().startsWith("multipart")) {
						List<MessagePart> mplist = mp.getParts();
						for (MessagePart lmp : mplist) {
							if (lmp.getMimeType().endsWith("plain")) {
								File tempDir = new File("/tmp/" + subject + "/");
								if (!tempDir.exists()) {
									// TODO: add date and from in this field
									FileUtils.forceMkdir(tempDir);
								}
								File bodyFile = new File(tempDir, from_prefix+"_body.txt");
								FileWriter fw = new FileWriter(bodyFile);
								fw.write(new String(Base64.decodeBase64(lmp
										.getBody().getData()), "UTF-8"));
								fw.close();
								files.add(bodyFile);
								System.out.println("Created File : "
										+ bodyFile.getPath());
							}
						}
					} else {
						if (mp.getBody().getAttachmentId() != null) {
							File tempDir = new File("/tmp/" + subject + "/");
							if (!tempDir.exists()) {
								// TODO: add date and from in this field
								FileUtils.forceMkdir(tempDir);
							}

							String filename = from_prefix+"_"+mp.getFilename();
							String messageId = m.getId();
							String attachmentId = mp.getBody()
									.getAttachmentId();

							MessagePartBody attach = service
									.users()
									.messages()
									.attachments()
									.get(CD_HACKATHON, messageId,
											attachmentId).execute();
							File file = new File(tempDir, filename);
							OutputStream os = new FileOutputStream(file);
							ByteArrayOutputStream baos = new ByteArrayOutputStream(
									mp.getBody().size());
							baos.write(Base64.decodeBase64(attach.getData()));
							baos.writeTo(os);
							baos.close();
							os.close();
							files.add(file);
							System.out.println("Created file : "
									+ file.getPath());
						}
					}
				}

				// call service to upload.
				String fromUserId = StringUtils
						.substringBetween(from, "<", ">");
				fromUserId = (fromUserId == null) ? from : fromUserId;
				String toUserId = StringUtils.substringBetween(to, "<", ">");
				toUserId = (toUserId == null) ? to : toUserId;
				String snippet = (StringUtils.isBlank(subject)) ? date
						: subject;
				ServiceUtils.uploadContents(fromUserId, toUserId, files, snippet);

				// mark message as read
				List<String> removeLabels = Lists.newArrayList("UNREAD",
						"INBOX", "TRASH");
				ModifyMessageRequest modmsgreq = new ModifyMessageRequest()
						.setRemoveLabelIds(removeLabels);
				Message message = service.users().messages()
						.modify(CD_HACKATHON, m.getId(), modmsgreq)
						.execute();
				System.out.println("Marked as unread : " + message.getId());

				// send reply message
				String replyString = "Your message has been successfully processed. Your mail and attachments have been stored in cloud drive.";

				Properties props = new Properties();
				Session session = Session.getDefaultInstance(props, null);
				MimeMessage email = new MimeMessage(session);
				InternetAddress tAddress = new InternetAddress(from);
				InternetAddress fAddress = new InternetAddress(
						CD_HACKATHON);
				email.setFrom(fAddress);
				email.addRecipient(javax.mail.Message.RecipientType.TO,
						tAddress);
				email.setSubject("Re: " + subject);
				email.setText(replyString);

				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				email.writeTo(bytes);
				String encodedEmail = Base64.encodeBase64URLSafeString(bytes
						.toByteArray());
				Message replyMessage = new Message();
				replyMessage.setRaw(encodedEmail);
				replyMessage.setThreadId(m.getThreadId());

				Message sentMsg = service.users().messages()
						.send(CD_HACKATHON, replyMessage).execute();
				System.out
						.println("Reply Message: " + sentMsg.toPrettyString());
			}
		}
	}
}
