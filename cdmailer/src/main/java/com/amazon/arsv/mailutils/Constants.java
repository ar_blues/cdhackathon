package com.amazon.arsv.mailutils;

import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;

public final class Constants {
	    // Check https://developers.google.com/gmail/api/auth/scopes for all
		// available scopes
		public static final String SCOPE = "https://www.googleapis.com/auth/gmail.modify";
		
		public static final String APP_NAME = "cdhackathon";
		
		// Email address of the user, or "me" can be used to represent the currently
		// authorized user.
		public static final String DEFAULT_USER = "me";
		
		//EMAIL Address of cdhackathon
		public static final String CD_HACKATHON = "cdhackathon@gmail.com";
		
		//EMAIL Address of cdhackathon-meeting
		public static final String CD_HACKATHON_MEETING = "cdhackathonmeeting@gmail.com";
		
		// Path to the client_secret.json file downloaded from the Developer Console
		public static final String CLIENT_SECRET_PATH = "/tmp/client_secret.json";

		public static GoogleClientSecrets CLIENT_SECRETS;

		//Dir where creds are stored
		public static final String CRED_DATA_DIR = "/tmp/fileDataStore";
		
		public static final String SERVICE_END_POINT = "http://abishekr-2.desktop.amazon.com:8000";
}
