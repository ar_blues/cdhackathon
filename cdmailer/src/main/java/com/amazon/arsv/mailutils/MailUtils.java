package com.amazon.arsv.mailutils;

import static com.amazon.arsv.mailutils.Constants.APP_NAME;
import static com.amazon.arsv.mailutils.Constants.CLIENT_SECRET_PATH;
import static com.amazon.arsv.mailutils.Constants.CRED_DATA_DIR;
import static com.amazon.arsv.mailutils.Constants.SCOPE;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleOAuthConstants;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;

public class MailUtils {

	private static GoogleClientSecrets clientSecrets;

	public static GoogleAuthorizationCodeFlow getFlow() throws IOException {
		HttpTransport httpTransport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();
		clientSecrets = GoogleClientSecrets.load(jsonFactory, new FileReader(
				CLIENT_SECRET_PATH));

		File dataDirectory = new File(CRED_DATA_DIR);
		FileUtils.forceMkdir(dataDirectory);

		DataStoreFactory dsf = new FileDataStoreFactory(dataDirectory);

		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, jsonFactory, clientSecrets, Arrays.asList(SCOPE))
				.setAccessType("offline").setApprovalPrompt("force")
				.setDataStoreFactory(dsf).build();
		return flow;

	}

	public static Credential getToken(final String userId) throws IOException {

		GoogleAuthorizationCodeFlow flow = getFlow();

		// check if present in data store
		Credential credential = flow.loadCredential(userId);

		if (credential == null) {
			// Allow user to authorize via url.
			String url = flow.newAuthorizationUrl()
					.setRedirectUri(GoogleOAuthConstants.OOB_REDIRECT_URI)
					.build();

			System.out.println("Enter authorizesduser's id (Is it " + userId
					+ "?) \n");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String authUserId = br.readLine();
			System.out
					.println("Please open the following URL in your browser then type"
							+ " the authorization code:\n" + url);
			// Read code entered by user.
			String authorizationCode = br.readLine();
			br.close();
			GoogleTokenResponse tokenResponse = flow
					.newTokenRequest(authorizationCode)
					.setRedirectUri(GoogleOAuthConstants.OOB_REDIRECT_URI)
					.execute();
			credential = flow.createAndStoreCredential(tokenResponse,
					authUserId);
		}

		return credential;
	}

	public static Gmail createEmailClient(final String code) throws IOException {

		HttpTransport httpTransport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();

		// Generate Credential using retrieved code.
		GoogleTokenResponse response = getFlow().newTokenRequest(code)
				.setRedirectUri(GoogleOAuthConstants.OOB_REDIRECT_URI)
				.execute();
		GoogleCredential credential = new GoogleCredential()
				.setFromTokenResponse(response);

		// Create a new authorized Gmail API client
		Gmail service = new Gmail.Builder(httpTransport, jsonFactory,
				credential).setApplicationName(APP_NAME).build();

		return service;

	}

	public static Gmail createEmailClient(final Credential credential)
			throws IOException {
		HttpTransport httpTransport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();
		Gmail service = new Gmail.Builder(httpTransport, jsonFactory,
				credential).setApplicationName(APP_NAME).build();
		return service;
	}

}
