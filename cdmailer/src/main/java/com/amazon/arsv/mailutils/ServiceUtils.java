package com.amazon.arsv.mailutils;

import static com.amazon.arsv.mailutils.Constants.SERVICE_END_POINT;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ServiceUtils {

	public static String getToken(final String userId, final String snippet)
			throws ClientProtocolException, IOException {
		String putUri = SERVICE_END_POINT + "/getToken?userId=" + userId + "&snippet=" + snippet;
		HttpPut put = new HttpPut(putUri);
		System.out.println(put.getURI());
		HttpResponse response = new DefaultHttpClient().execute(put);
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		response.getEntity().writeTo(bytes);
		String operationResponse = new String(bytes.toByteArray());
		String resp_trim = StringUtils.substringBetween(operationResponse, "<token>",
				"</token>");
		return resp_trim;
	}

	public static Map<String,String> uploadContents(final String from, final String to,
			final List<File> files, final String snippet)
			throws ClientProtocolException, IOException {
		String token = StringUtils.substringBetween(to, "+", "@");
		token = (token==null) ? "" : token;
		String format = "/postData?filename=%s&token=%s&source=%s&snippet=%s";
		Map<String,String> result = Maps.newTreeMap();
		for(File file : files) {
			String params = String.format(format, file.getName(),token,from,snippet);
			String putUri = SERVICE_END_POINT+params;
			InputStream inFile = new FileInputStream(file);
			HttpPut put = new HttpPut(putUri);
			put.setEntity(new InputStreamEntity(inFile, -1));  // -1 means take the whole stream
			
			System.out.println(put.getURI());
			HttpResponse response = new DefaultHttpClient().execute(put);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			response.getEntity().writeTo(bytes);
			String operationResponse = new String(bytes.toByteArray());
			result.put(file.getName(), operationResponse);
		}
		return result;
	}

	public static void main(String[] args) throws ClientProtocolException,
			IOException {
		
		System.out.println(getToken("userId", "snippet"));
		
		File file1 = new File("/tmp/test_mailto/body.txt");
		File file2 = new File("/tmp/test_mailto_again/body.txt");
		
		System.out.println(uploadContents("b@pqr.com","aasd+345dsfsr@xyz.com", Lists.newArrayList(file1,file2), "snippet"));
	}
}
